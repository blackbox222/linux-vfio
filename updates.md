# Updates and fixes I've done over time

List of various things that I have had to do over time with Arch updates, etc. to keep the VM working.

## VM not booting/libvirtd hang due to apparent issue with hugepages on mainline Arch kernel

libvirtd completely freezes and can't even be `kill -9`'ed (this also prevents `ps` from working, nor can the system be shut down cleanly since it causes a hang when listing `/proc`).  Looking at kernel stacktrace for the stuck process implied that it may have something to do with hugepages.

I ended up having to switch to the LTS kernel to work around this, but I expect that workaround won't last forever.

TODO: investigate this.

## VM not booting due to PCI device topology changes

TODO: investigate why this happens

Sometimes after a BIOS or (rarely) a kernel update, the PCI device topology changes, causing the PCI address of the cards in the system to change, which results in the VM not booting and possibly a libvirtd freeze.

`dmesg` had some output relating to binding/unbinding vfio-pci devices which hinted at the issue.

This required reconfiguring the passed through devices in the domain xml.

## USB controller passthrough for audio interface latency

Passing through an audio interface as an individual USB device doesn't provide acceptable latency, so I had to pass through an entire USB controller which luckily has its own IOMMU group on my motherboard:

```
<hostdev mode="subsystem" type="pci" managed="yes">
  <driver name="vfio"/>
  <source>
    <address domain="0x0000" bus="0x0d" slot="0x00" function="0x3"/>
  </source>
  <alias name="hostdev2"/>
  <address type="pci" domain="0x0000" bus="0x0c" slot="0x00" function="0x0"/>
</hostdev>
```

## Avoiding failures starting the VM if a redirected USB device isn't present

Use `startupPolicy="optional"` e.g.:

```
<hostdev mode="subsystem" type="usb" managed="yes">
  <source startupPolicy="optional" missing="yes">
    <vendor id="0x1532"/>
    <product id="0x0207"/>
  </source>
  <alias name="hostdev1"/>
  <address type="usb" bus="0" port="3"/>
</hostdev>
```

## BT controller USB redirection

The (USB) Bluetooth controller built into my motherboard isn't in its own IOMMU group, so I redirected the individual device to the guest:

```
<hostdev mode="subsystem" type="usb" managed="yes">
  <source>
    <vendor id="0x8087"/>
    <product id="0x0029"/>
    <address bus="1" device="18"/>
  </source>
  <alias name="hostdev0"/>
  <address type="usb" bus="0" port="4"/>
</hostdev>
```

However, this doesn't work on its own, the driver in windows stops working if you try to actually pair bluetooth devices without special qemu capabilities settings:

```
<qemu:capabilities>
  <qemu:del capability='usb-host.hostdevice'/>
</qemu:capabilities>
```

## 5.1/7.1 sound to the host

This probably doesn't matter anymore due to the USB controller passthrough above, nor does it work correctly if the host isn't actually set up for multichannel sound (i.e. it won't downmix, it will just play garbage through the speakers)

```
<sound model='usb' multichannel='yes'>
  <audio id='1'/>
  <alias name='sound0'/>
  <address type='usb' bus='0' port='5'/>
</sound>
<audio id='1' type='pulseaudio' serverName='/run/user/1000/pulse/native'>
  <input mixingEngine='yes'/>
  <output mixingEngine='yes'/>
</audio>
```

TODO: sometimes audio freezes other apps on the host - maybe switch back to intel emulated PCI audio?

## OVMF firmware 2M -> 4M change

The VM stopped booting with some error about not being able to locate `OVMF_CODE.fd` - they [changed](https://salsa.debian.org/qemu-team/edk2/-/blob/debian/latest/debian/howto-2M-to-4M-migration.md) the size of the firmware file from 2M to 4M and dropped CSM support, so this required updating the path to the file and setting firmware image type to `pflash` instead of `rom`.

While I was in here I updated the emulated PC model to the newest since the VM was using a deprecated version.

```
<os>
  <type arch='x86_64' machine='pc-q35-9.2'>hvm</type>
  <loader readonly='yes' type='pflash' format='raw'>/usr/share/edk2-ovmf/x64/OVMF_CODE.4m.fd</loader>
  <nvram format='raw'>/var/lib/libvirt/qemu/nvram/win10_VARS.fd</nvram>
  <bootmenu enable='no'/>
</os>
```

I did not bother with running the script described in that guide to convert the nvram image from 2M -> 4M, everything just worked.

# Custom EDID to override it getting wrong EDID from projector / HDMI splitter

The computer is plugged into an HDMI splitter with eARC extraction to solve an issue where my projector introduces audio latency into the eARC signal to a soundbar if it's plugged into the eARC port of the projector.  Unfortunately this splitter seems to be copying the EDID of the soundbar rather than the projector's to the computer input (1080p), regardless of whether the projector is on or not.

To force 4k I had to create a custom resolution in the NV control panel for 3840x2160@60hz.

TODO: figure out HDR since my projector supports it
