# Linux Desktop Setup/Tweaks

This details my experience setting up a Linux desktop from scratch in 2021 using Arch Linux.  I've been using Linux since 2003 or so, but most of that has been in headless/server environments and it's been a while since I tried setting up a proper desktop (i.e. on a "batteries not included" distro such as Slackware where you have to make all the decisions yourself), I figured this would be a good way to re-acquaint myself with what's changed and how it all fits together.

None of this is an attempt at trying to suggest what *you personally* should do, do whatever you think will work best for you.  I would greatly appreciate suggestions and improvements, especially ones that get closer to best practices; for example, a lot of these directions include things such as "log out and back in to effect some sort of change", this seemed faster than trying to figure out the `systemctl` invocation to restart some service, especially with relatively "global" settings such as DPI.

## Basic components

Everything was installed from official Arch repos or AUR unless otherwise stated.  Some of my "false starts" are detailed at the end of this doc.

* Display server: X (wayland isn't that well supported *yet*)
* Video drivers: `nvidia` proprietary drivers
* Display/login manager: `lightdm` with the `lightdm-gtk-greeter`
* Desktop environment: `xfce4` (experimented with a few others as well.  Installing `gnome` might be needed for the theme I'm using and for other programs to behave nicely. I'm also using the `caja` file manager from MATE).

My goal was to get something that doesn't look too visually jarring/different from Windows while it is running (i.e. so I could have the Windows VM side by side with the Linux desktop).

## HiDPI

I mostly followed the HiDPI guide from the Arch wiki, with the following tweaks:

* Set window scaling to 2x in settings > appearance > settings
* Enabled full font hinting and antialiasing; custom DPI of 96 (this was important to stop non-xfce apps from doubling scaling)
* Style: adwaita-dark

Instead of creating an `~/.xinitrc` as the wiki mentions, since this breaks a few other things like for example, as I found, `~/.Xmodmap` won't be applied at login unless you copy some boilerplate from `/etc/X11/xinit/xinitrc`, I created an `~/.xprofile` with the following:

```
export GDK_DPI_SCALE=0.75
export QT_SCALE_FACTOR=1.5
```

The values here are probably a matter of personal taste, I just wanted something semi-close to Windows DPI. Log out and back in, adjust system font sizes again since GDK_DPI_SCALE causes most xfce components to change their scaling, test Qt apps, etc.

Logged out and in repeatedly to test the different DPI settings (since `~/.xprofile` is evaluated when the desktop manager starts a new session).

### Fixes for specific programs that don't respect the above settings

* Spotify: press `Ctrl` and `+` at the same time to increase font size (this doesn't obey any system DPI settings as far as I'm aware).

### HiDPI on the lightdm screen

The fonts are way too tiny on the login screen out of the box.

* `pacman -S lightdm-gtk-greeter-settings` and then in this program, set a *larger* DPI than 96
  * it's not perfect but it's good enough for the negligible amount of time I'm going to spend at the login screen

## Bottom panel/"taskbar" setup

Delete all the panels except the bottom one. Set a size of 32 pixels and dialed down the opacity slightly.

* Window list plugin: `xfce4-docklike-plugin` (pretty close to the Windows taskbar icons).  I didn't care for dockbarx
  * This has a few minor idiosyncracies, like needing to hold `Ctrl` to reorder pinned icons, and determining which windows "belong" to a pinned icon appears to be based on `StartupWMClass=` in the .desktop file matching the window class exactly (run `xprop WM_CLASS` and click on a window, the *second* string listed is the class (the first one is "instance" which some other window managers seem to group windows by instead...)
* App menu plugin: `xfce4-whiskermenu-plugin` (pretty close to the Windows one, this also partly solved the blurry icon issue described below).  I set the icon to `start-here`.

I did a few other minor tweaks like adjusting the clock display, etc...

## Blurry icons

Apparently xfce scales up the 16x16 icons rather than using a higher res icon when DPI scaling is set to 2x.

* Icon theme: `papirus-icon-theme`
  * this appears to be the default in Manjaro, and it includes tons of icons for various apps/system icons/etc.
* File manager: ended up just using `caja` from the MATE desktop since `thunar` has the same problem
* Desktop: haven't bothered...

## Monitor switching issues

If monitors go to sleep/get switched to different inputs the desktop would get reconfigured strangely (e.g. mirroring getting turned on).  In settings > displays:

* Configure new displays when connected: on
* Automatically enable profiles when new display is connected: on
* Setup the screen geometry the way I wanted it and then created and saved a monitor profile in the section above

## Changing caps lock into escape

I use vim/vim bindings in programs that support them, and caps lock is a pretty useless key to me, so I normally map this to escape.  Xfce doesn't seem to have an option to do this in settings, but you can do it using xmodmap:

Created `~/.Xmodmap`:

```
remove Lock = Caps_Lock
keysym Caps_Lock = Escape
add Lock = Caps_Lock
```

Ran `xmodmap ~/.Xmodmap` to apply the settings without logging out and back in.

## Special keys

In settings > keyboard > application shortcuts, configured the `Calculator` key to launch `gnome-calculator` since it didn't seem to do anything by default.

## Audio problems

I had quite a few annoyances with audio not working as it should.

### Sound getting randomly muted by other programs

Edit `/etc/pulse/default.pa` and comment out `load-module module-role-cork`, log out and back in to restart pulseaudio.  Apparently this module is used to mute other programs when a "phone" app wants to use audio (virtual machine audio is somehow included in this "phone" category).

### USB mic not working until reconnected, output devices randomly changing, etc.

I have a Blue Yeti USB mic and if the device is plugged in when the system boots, it doesn't work until it's unplugged and replugged (it gets detected but there is no sound from the mic).  This isn't great cause it causes the output device to change, since it can output sound from the mic's headphone jack, I want to use the motherboard audio instead.

I'm not really sure which of these things solved the problem, maybe all of them (it seemed to start working out of the clear blue after a reboot and I'm not sure which thing I tried last):

* Installed `alsa-firmware`
* Commented out `load-module module-suspend-on-idle` in `/etc/pulse/default.pa`
* In the pulseaudio volume control, disabled all audio devices I wasn't using such as the video card HDMI out and set the following profiles for the mic and mobo audio:
  * Blue Microphones (USB mic) = Analog Stereo Input
  * Starship/Matisse HD Audio Controller (mobo sound) = Analog Stereo Output
* I also clicked the lock icon for all the devices on this screen and set the blue mic as the "fallback" device on the input devices screen.

## Other tweaks

(not sure if this is necessary for Xfce) I had to remove `network-manager-applet` to avoid unusable tray icons being displayed (as my network is configured directly through `udhcpd` instead and `NetworkManager` isn't used)

### Fonts

I followed the procedure [here](https://wiki.archlinux.org/title/Microsoft_fonts#Extracting_fonts_from_a_Windows_ISO) to install fonts from a Windows ISO, and installed a bunch of other commonly used fonts: `ttf-dejavu`, `ttf-croscore` as well as `wqy-zenhei` recommended font for Steam on Linux.

I didn't bother with the fontconfig rules since I don't necessarily want to use them all the time, I just want programs that default to them to look "normal".

### Logitech wireless keyboard/mouse specific stuff

I use a Logitech Ergo K860 keyboard and MX Master mouse.  I installed `solaar` which is a pretty nice UI to configure these and show their battery levels, show notifications when devices connect, etc.

I then added a startup entry in settings > session and startup to run `solaar -w hide` at login (this starts it in the tray but doesn't bring up the main window).

I also used the rule editor to map the "gesture" button (under the thumb) on the MX Master to "mouse button 6", i.e. to allow it being used as a generic mouse button rather than executing a "gesture" or pressing other keys (buttons 1-5 already correspond to the other buttons on the mouse):

* In the solaar UI, set Key/Button Diversion for "Mouse Gesture Button" to "Diverted" (click the padlock icon a couple times to enable editing the setting)
  * I also made sure Mouse Gestures were off
* Install `xdotool` which allows generating arbitrary X events (since none of the built in actions allow for generating a mouse/key down event without the corresponding up event)
* Open the rule editor and add two new user defined rules to generate mouse events when it is pressed/released:
  * Rule
    * Key: Mouse Gesture Button (key down)
    * Execute: `xdotool mousedown 6` (each argument needs to go in its own field)
  * Rule
    * Key: Mouse Gesture Button (key up)
    * Execute: `xdotool mouseup 6`

I'm using this for a PTT key for voice chat apps.  I doubt executing `xdotool` like this will be fast enough for something that requires less latency (e.g. to use as a binding in games)

### Forcing certain programs to appear on certain displays

It seems that xfce tries to place windows on the display they are launched on and this behavior can't be turned off (as far as I can tell).  In my case I only cared about this for one (fullscreen) program, the Looking Glass client that I used with Windows.

Install `devilspie2`, and create a config script for it in `~/.config/devilspie2/looking-glass-client.lua` (for example I always want this program on the second monitor):

```
if (get_window_class() == "looking-glass-client") then
  set_window_geometry(3840, 0, 3840, 2160)
end
```

I then added `devilspie2` as a startup entry to run the program when I log in.

## Things I tried before settling on lightdm/xfce

While I am no stranger to Linux per se (I have been using Linux since the days of Slackware 9.1 or so) most of this use in the last few years has been in a headless server setting, and I haven't bothered configuring a Linux desktop in a while, so I went through a series of false starts.  I document the basic steps here since I probably did steps that are necessary for things to work regardless (such as setting up pulseaudio):

* I first tried KDE out again with `sddm`, `plasma`, `kde-applications`.  It seemed sort of laggy and clunky, a lot of the default apps were quite broken (konqueror kept opening links on pages in a new window, etc).
* Sound wasn't working until I installed `alsa-utils`, `alsa-plugins`, `pulseaudio`.
* I set up AUR and installed `google-chrome` manually:
    * `sudo pacman -S git base-devel`
    * cloned the AUR repo and ran `makepkg -si` in it
    * (had I installed `yay` first, which I eventually did, I could have avoided this step)
* I think running Chrome at this point is where I realized plasma has some performance issues, and nothing (especially DPI) was consistent in KDE vs Gnome/GTK and tried `pantheon` which didn't function at all, so I gave up and uninstalled both pantheon and kde.
* I tried Xfce4 which was always my preferred WM in the past: `sudo pacman -S xfce4 xfce4-goodies`, but I recall having some problems configuring HiDPI (and then breaking my settings in such a way that prevented it from starting until I deleted all xfce-related config files).
* Installed `spotify` from AUR.
* Continued trying other environments: `cinnamon`, `enlightenment` didn't really appeal to me at all since they had the same "everything looks out of place" feel with apps that aren't designed for them.
* For a couple of weeks, I ended up just settling for GNOME with `budgie-desktop`; switching display managers to `gdm` and removing `sddm` and the other aborted attempts at trying a window manager except for Xfce.
* I tried `mate` as well and while I like some of the MATE apps such as `caja` a little more than the Xfce ones, there were a few glaring bugs with this one (plus I couldn't find a way to set up the taskbar in a way that I liked).

At this point I declared the "find a window manager that works for me" mission complete and moved onto the next part, actually setting up Steam and trying games in Proton.

## Setting up for games on Linux (with Proton)

### 32-bit graphics libraries for running 32-bit games

Enable `multilib` by uncommenting it in `/etc/pacman.conf` and then run:
```
sudo pacman -Syu
sudo pacman -S lib32-nvidia-utils
```

### Steam

I installed Steam and a few other utils just to do some basic actual 3d tests like `glxgears`, `teapot`, etc just to prove everything was working in Linux:

`sudo pacman -S steam mesa-demos`

Steam ran fine here.  I more or less straight away enabled Steam Play with the experimental build of Proton, and installed a few games.  I read through protondb.com to see which flags to use or whether certain proton versions are needed, etc.  Many games seemed to work well enough, though FPS was still noticeably worse at 4K compared to Windows, so I decided to go with a dual GPU setup.