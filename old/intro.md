## Intro

This all began as a set of notes I wrote for myself, should I come back to this in a few years and want to try to remember how to do this again, but I've tried to adapt it into a more general guide (at least for AMD Ryzen 9/X570 systems with NVIDIA GPUs to get a general understanding of what this involves).

All the same, I don't guarantee that (even if you happened to have the exact same hardware) that you will be able to replicate my results perfectly.  Some of the advice might be completely faulty; I started writing these notes after the system was mostly set up and much it is being recalled from memory (with the help of logs from `journalctl`).  **I encourage you to do your own homework, read other guides, read the Arch wiki, read r/VFIO on reddit, etc rather than trying to blindly follow this guide.**

While I will try to "handhold" a bit more through the "tricky" parts (much of which are new to me), I assume you have a fairly thorough understanding of PC hardware (i.e. PCIe standards and how the lanes connect to the processor/chipset, cache and memory locality, boot process, troubleshooting, etc) as well as Linux and Windows usage and system administration and will not go into extreme detail for the more "vanilla" aspects of the setup.

## Hardware

* CPU: AMD Ryzen 9 5950X
* RAM: [G.SKILL Ripjaws V Series 64GB (2 x 32GB) DDR4-4400](https://www.gskill.com/product/165/184/1618389667/F4-4400C19D-64GVK)
    * In retrospect, I should have researched this kit more and probably wouldn't recommend it for an AMD build.  Based on the QVL list, the XMP profile looks like it's only really been tested with Intel boards/CPUs.  As such I wasn't able to get this to run at the advertised 4400 GT/s, but runs stable at 4000 @ 1.4V.
        * The stock voltage of this module is really high (1.5V!).  I was able to get it up to ~4200 at stock voltage, but I felt uncomfortable with that high of a DRAM voltage (not to mention apparently the infinity fabric clock is supposed to be a multiple of the DRAM clock, I don't know that much about AMD/Ryzen OCing and so 4000 is good enough for me :)
    * DRAM chips are SK Hynix, which apparently is on the more losing end of the memory OC "lottery".
* Mobo: [ASUS ROG Crosshair VIII Dark Hero](https://rog.asus.com/us/motherboards/rog-crosshair/rog-crosshair-viii-dark-hero-model/)
    * I was mainly interested in this board since I'm going for a sleeper build and it's one of the few X570 boards with a passively cooled chipset, and I wanted something with 2 x16 slots / 2 M.2 slots
    * It also turns out to have pretty decent default IOMMU groups for VFIO.
* NVMe:
    * Samsung 980 PRO 2TB (this is a Gen4 device, so I have it in the first slot nearest the CPU)
    * Samsung 960 EVO 1TB (lower slot near the southbridge)
* GPUs:
    * Host (boot): EVGA GTX 1070 SC 8GB - in the first slot closest to the CPU
    * Guest: MSI RTX 3090 SUPRIM X 24GB - in the second x16 slot
    * This motherboard doesn't seem to allow selecting the boot GPU, so the GPU that will be primarily used with Linux needs to go in the first slot, otherwise video gets messed up and freezes if the guest GPU is plugged into anything.
* Other stuff that shouldn't affect VFIO but listed for completeness' sake:
    * PSU: EVGA Supernova 1000 T2 (1000W, 80+ Titanium).  This is probably overkill but I really don't want my house to burn down if/when my PSU fails and I need enough headroom for two GPUs.
    * CPU Cooler: NZXT Kraken X63.  I re-pasted it myself with Arctic MX-5 rather than using the pre-applied thermal pad.
    * Old case and SSD that were sitting around: NZXT H500 and SanDisk Ultra II 1TB
        * The airflow in this case is kind of terrible and there are no vertical GPU mounts (especially with 2 GPUs), so I can't recommend it (but "aesthetics" and "what I already have sitting around" won out).
    * Display: 2x 4K @ 60hz LG and Dell monitors that I already had

## Software
* OS:
    * Host: Arch Linux (2021.10.01)
    * Guest: Windows 10 Enterprise (21H1, Sep '21 update).  I didn't want to bother with Win11 and potential bugs or the extra setup needed for Secure Boot / TPM 2.0 enablement in a VM.  Other editions of Win10 will probably work fine if you don't have access to Enterprise.

## Background (the initial build with Win10 on bare metal)

Windows was actually installed on the old SanDisk SSD from an older Intel 7700K build, so this got carried along for the ride with the new build at first.

I hadn't planned to do VFIO initially, but after using Win10 long enough I decided I really didn't want to deal with the constant issues and idiosyncracies of Windows, this is what I remember doing:

* Updated BIOS to version 3801 (07/30/2021) via USB, and then booted into the original Windows 10 Pro install on the SSD.
* As mentioned, I enabled XMP/DOCP but set RAM speed to 4000 and voltage to 1.4V.  Left other clocks / voltages at stock since I am not that big into OCing, I mostly just wanted to get as close to the vendor's "stock" specs as I could.
* Turned on virtualization and a bunch of other features such as SR-IOV, 64bit PCI address decoding, resizable BAR, disabled wifi (since I don't need it), etc.

I started thinking about VFIO more now that several people had written guides for doing it with a single-GPU passthrough setup, and since I already have to use Linux regularly for work, I thought it would be nice to see how well games work on Proton, and for those that don't work well, just limit running Windows to these specific things that can't work otherwise.

## Hardware prerequisites/gotchas for VFIO on Linux

I initially started with a single-GPU passthrough setup, but ended up switching to multiple GPUs to avoid the annoyance of having to kill my session to stop and unbind the drivers (which makes this not that much of an improvement over simply dual booting, anyway).  I'll cover the base Linux system setup (i.e. up to having a working graphics environment prior to anything VFIO or virtualization related).

* As previously mentioned, the host / boot GPU *had* to be moved to the first PCIe x16 slot.  Not doing so caused problems and video/boot failure if the guest GPU was connected to a monitor.
* The primary monitors were connected to the host 1070 via DisplayPort.
    * I previously had issues with the max resolution being 4K@30hz using HDMI cables; DP cables fixed this problem.
* The guest 3090 was connected to an unused HDMI port on my second monitor.
    * Some sort of active monitor connection is required for the Nvidia GPU to initialize its framebuffer.  I later replaced this with EDID dummy plugs which I will get into why later.