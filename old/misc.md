* This assumes that your board has more than one PCIe x16 slot, and both are connected directly to the CPU (not the southbridge), as well as sufficient space to actually fit all the cards.
    * Note that most Ryzen CPUs only have 24 PCIe lanes, 4 of which go to the chipset.  This will result in PCIe bifurcation, where each card runs in x8 mode.  In my primary GPU use case (gaming) this really isn't that big of a deal and probably results in a small (on the other of a few percent) effective slowdown that I can live with, but might be disastrous for other applications (e.g. CUDA/OpenCL/etc) that need as much VRAM bandwidth as they can get.
    * If you need more PCIe lanes to run more than one video card in x16 mode the only real option is to invest in a workstation or server grade CPU/mobo that has more lanes, e.g. Threadripper, EPYC, etc., or perhaps use a CPU that has an iGPU.
* Depending on the system setup and cards used, you may need to invest in a beefier power supply (mine is 1000W to be on the safe side to run both the 1070 and 3090).
* At least on my mobo and BIOS version, there is no (functioning) option 
to select which GPU to use at boot time, so the GPU that is to be used with Linux needs to be physically plugged into the first PCIe slot.
    * Failing to do this resulted in video glitching/system freezing when starting the bootloader (I would get a really low res text mode screen `Welcome to GRUB` and the system would freeze).


### Monitor/cabling/EDID setup

In the case of the 3090 at least, but I suspect this is true of most consumer GPUs, the GPU hardware will not initialize or function at all unless the GPU can see an EDID on at least one port, at least while the system is booting; this is the information transmitted by the monitor such as the list of resolutions it supports, manufacturer/model name, signal timing and color profile data, etc. essentially telling the GPU what is connected to it.

In theory, if your monitor has unused inputs, you should be able to run an HDMI or DP cable to the second GPU.  However, this assumes the monitor continues transmitting an EDID for inactive inputs, since I plan to use Looking Glass to display the Windows desktop in a window on the Linux desktop so I can use Linux and Windows simultaneously, I don't expect to switch the monitor to the other card very often.  Neither of my monitors appear to do this; I assume this is to allow the GPU to go to low power mode when it's not being displayed.

You can test whether this is the case by switching inputs while the machine is running; if this causes the system to resize the desktop or otherwise act like the monitor has been disconnected, your monitor has the same problem.  Allegedly some monitors will continue sending the EDID for certain types of ports, or disabling sleep-related settings in the monitor itself might fix the problem, but none of these ideas worked for either of the monitors I own.

Ultimately I ended up just buying HDMI dummy plugs; this is a small device that plugs into the HDMI port and does nothing beyond continuously transmit an EDID so the video card thinks it's always connected to a "real", active monitor.

[These ones I found on Amazon ($10 USD for two of them)](https://www.amazon.com/dp/B07FB4VJL9) seemed to work fine for me.  One downside is that this is a *very* basic device; it transmits a fixed EDID that fakes some sort of AOC monitor that tops out at 4K@60 and no HDR color formats (that said, someone [tore apart a very similar device](https://goughlui.com/2019/06/02/review-teardown-generic-hdmi-cheater-dummy-plug-4k-uhd/) and saw it was a very standard sort of EEPROM, so I suppose you could do the same and reprogram it with whatever EDID you want if you were so determined and own the appropriate hardware), nor is HDCP supported, so I guess I won't be watching Netflix/etc on the Windows guest (not that I really care to do this on my PC in the first place).  If you need HDR, >60hz refresh rates, HDCP, etc., a better solution that doesn't involve connecting to a physical monitor would probably be a more expensive EDID manager or HDMI matrix switch.

While I was waiting for the dummy plugs to show up on my doorstep, I lived with manually switching the inputs every time I rebooted Windows (since the HDMI connection only needs to be active while the GPU drivers initialize), and ended up writing a script to auto-switch them for me; you might wonder how this is even possible, but I'll get to that in a little bit.


### Auto-switching monitor inputs during VM startup with ddcutil

As previously mentioned, while waiting for the dummy HDMI plugs to arrive, I played around with auto-switching monitor inputs in software.  I did away with this script afterwards, but I keep it here for posterity (or in case latency with Looking Glass ends up being a problem in some programs where a physical monitor connection to the VM hardware might be desirable).

Most (reasonably modern) monitors support a little known feature called [DDC/CI](https://en.wikipedia.org/wiki/Display_Data_Channel#DDC/CI), which (in theory) allows the host to send commands to the monitor, including selecting inputs, changing brightness, HDMI audio output volume, among other things.  In my experience, not all monitors seem to fully implement this, or implement it strangely; for example, changing inputs only seemed to work some of the time, and only if the command came from the currently active input on my LG monitor.  My Dell monitor on the other hand seemed to implement it flawlessly; I did have to go into the monitor settings menu and enable DDC/CI, which was off by default.

Install `ddcutil`: `sudo pacman -S ddcutil`

First we need to figure out which monitor is what:

```
sudo modprobe i2c-dev  # note that this driver is needed for ddcutil to work
sudo ddcutil detect  # ddcutil needs to be run as root in order to access /dev/i2c-*
```

This should output something similar to the following:
```
Display 1
   I2C bus:  /dev/i2c-7
   EDID synopsis:
      Mfg id:               GSM
      Model:                LG HDR 4K
..snip for brevity..

Display 2
   I2C bus:  /dev/i2c-8
   EDID synopsis:
      Mfg id:               DEL
      Model:                DELL U2718Q
..snip for brevity..
```

In my case, the Dell monitor is the one I want to use with the VM (and the only one that has fully working DDC/CI, anyway), so display 2 it is.  
For the rest of the commands, `ddcutil` has a few different flags to select which monitor to control, `-d 2` selects the second monitor by number.  I ended up using `-g DEL` to always select based on manufacturer, so it will still pick the correct physical monitor even if the DP cables were to end up getting physically swapped next time I mess with hardware in the PC (since I guess I can't be bothered to label the cables).  Let's see which inputs the monitor supports with `sudo ddcutil -g DEL capabilities`, which spits out a ton of information:

```
Model: U2718Q
MCCS version: 2.1
Commands:
   Op Code: 01 (VCP Request)
   Op Code: 02 (VCP Response)
   Op Code: 03 (VCP Set)
   Op Code: 07 (Timing Request)
   Op Code: 0C (Save Settings)
   Op Code: E3 (Capabilities Reply)
   Op Code: F3 (Capabilities Request)
VCP Features:
   Feature: 02 (New control value)
   Feature: 04 (Restore factory defaults)
   Feature: 05 (Restore factory brightness/contrast defaults)
   Feature: 08 (Restore color defaults)
   Feature: 10 (Brightness)
   Feature: 12 (Contrast)
..snip for brevity..
   Feature: 60 (Input Source)
      Values:
         11: HDMI-1
         0f: DisplayPort-1
         10: DisplayPort-2
..snip, a whole bunch more info we don't care about..
```

Interestingly, there seem to be a whole bunch of features that I'm not sure are even exposed in the monitor's menus, which is interesting, but `Feature: 60 (Input Source)` is what we care about, since this gives us a list of values for each of the inputs.

Note that these are all hexadecimal numbers and apparently `0x60` is defined by the standard to always mean the selected input, but the input source *values* seem to vary from monitor to monitor.  I also seem to recall reading somewhere while researching `ddcutil` that the listed values don't always line up with the actual input ports they refer to, so you may need to do some trial-and-error switching inputs and getting the current value if this is the case for your monitor).

We can read the current value with `sudo ddcutil -g DEL getvcp 0x60`:

```
VCP code 0x60 (Input Source                  ): DisplayPort-1 (sl=0x0f)
```

More interestingly, we can immediately switch the monitor input to one of the values from the previous list: `sudo ddcutil -g DEL setvcp 0x60 0x11` (e.g. HDMI on my monitor)

If this causes the monitor to switch inputs, congratulations, your monitor properly supports DDC/CI.  Unfortunately, this did not work reliably on my LG monitor, sometimes it would do nothing, and even when it did, it would immediately lose the DDC connection to the computer and would stop being detected by `ddcutil` so it was impossible to programmatically switch back.  My Dell monitor seems to keep the DDC connection alive, at least for a short period of time after switching inputs.

With all the pieces in place, I added a hook script to run immediately after the VM starts up to switch to the HDMI input, wait for the OS to boot and initialize the GPU driver, and then switch back to the DP input:

`/etc/libvirt/hooks/qemu.d/win10/started/begin/switch_displays.sh`:

```
#!/bin/bash
# ensure the i2c driver is loaded
modprobe i2c-dev

# switch second monitor to HDMI (so the GPU initializes)...
ddcutil -g DEL setvcp 0x60 0x11

# wait for windows to start...
sleep 12

# switch back to DP input (Linux GPU)
ddcutil -g DEL setvcp 0x60 0x10
```

12 seconds incidentally seemed to be just enough time to reach the login screen before switching back.  There's only one slight problem with all of this... we haven't actually set up the GPU passthrough, so none of this will actually work until we do that.


## TODO / other things I'd like to try

* Clover (bootloader)? Seems to have a GUI and supports ACPI hacks for booting macOS
* Custom graphics/fonts in GRUB?
* See if MATE is better for me than budgie?
* Fix issue with scrollwheel being "touchy"?
* Fix lockscreen issues (not locking, glitching, breaking Looking Glass, etc?)
* Dual monitors in Windows (i.e. making Windows display geometry match Linux, two instances of Looking Glass, or maybe "surround" mode in the NV driver?)
* Fix cursor issues when Looking Glass doesn't have focus (as [this person](https://www.reddit.com/r/VFIO/comments/q00vw7/complete_unification/) appears to have done so)
* Better integration between desktops? (e.g. desktop entries that start Windows programs, alt-tab to a Windows program by way of "fake" windows on Linux, etc)
* NUMA settings, changing cpuset at runtime in response to load, etc?
* Benchmark qcow2 vs virtio-fs vs a raw device, etc
* Making evdev work with my "combined" keyboard/mouse?

### Getting `decode-dimms` to work

This [post](https://damieng.com/blog/2020/02/08/ddr4-ram-spd-linux) has some useful detail about getting `decode-dimms` to work for DDR4 on Linux.

**TL;DR** This worked for me but probably depends on your chipset.  Proceed with caution, etc.
```
# modprobe ee1004
```

TODO

### Installing old version of TeamSpeak

For a variety of reasons, I have to connect to a really old TS3 server.

Old versions can be found [here](http://dl.4players.de/ts/releases/).  I ended up with 3.1.10 as the most modern version that was supported by the server.

`sudo pacman -S libpng12`

### HiDPI scaling fixes

TODO

### Pulseaudio randomly muting (i.e. system or program A makes a sound, program B mutes)

Edit `/etc/pulse/default.pa` ... disable the "Cork" plugin

TODO

### Capslock as escape and similar keybind overrides not working in vscode

From [linuxdev.io](https://linuxdev.io/howto-fix-caps-lock-escape-swap-not-working-in-vs-code/) set Preferences > Settings > Application > Keyboard > Dispatch, select keyCode.