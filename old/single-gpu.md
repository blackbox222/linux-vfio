
* Especially in the single-GPU passthrough case, it's easy to end up in a situation where it's impossible to interact with the host (because video/input are broken), making it hard to debug.  If you have access to another device that can use ssh (even your phone), install OpenSSH:

    ```
    sudo pacman -S openssh
    sudo systemctl enable --now sshd.service
    ```

    (if you are using DHCP, take note of the system's IP address using `ip addr` to know which machine to ssh to!)


## Single GPU Passthrough with VFIO (i.e. switching the GPU between Linux and Windows)

The primary guides I followed were the Arch wiki's [PCI passthrough](https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF) guide, along with [joeknock90's Single GPU Passthrough on Linux](https://github.com/joeknock90/Single-GPU-Passthrough) guide, as well as a few [others](https://github.com/QaidVoid/Complete-Single-GPU-Passthrough), including r/vfio on Reddit.  As before, I made some adaptations/changes from these which will be called out below.

### Caveats / things to know

* I include this more for completeness' sake rather than anything, in order to showcase what I've tried.  Single GPU passthrough assumes a **complete** shutdown of anything Linux video-output-related; your X session and all graphical programs will have to be terminated before the VM can run, i.e. pretty much the only things that can stay working are daemons that run in the background (e.g. accessing programs run with `screen` or `tmux` via SSH that don't need a physical console).  While it's faster than dual booting, it's probably less than ideal if you want to use the Windows VM more than semi-infrequently or switch between the Linux and Windows desktops often.
    * Maybe there's a way around this for non-GPU-accelerated programs at least; I didn't bother.  Even "simple" programs like a web browser (or many programs that are actually a web browser such as `electron` under the hood) use basic hardware acceleration.
    * I ended up giving up and just installing an older graphics card (GTX 1070) to use for the Linux host.  I would strongly recommend doing the same (even buying a used/cheap one if necessary) and skip this part and move onto the *Dedicated GPU Passthrough* section below.
    * If you are dead-set on this method, you should setup SSH as previously suggested and you might also try setting up something like [TigerVNC](https://wiki.archlinux.org/title/TigerVNC) to allow running Linux graphical programs from "within" Windows (albeit with severe input lag and without hardware acceleration at least when I tried it).
    
### Hook scripts to switch the GPU between host and guest

In order to move the GPU to the guest, before the guest VM is started, anything accessing the GPU needs to be unloaded/stopped, which include all of the following services:
* X server (which by extension includes the logged in graphical session)
* Graphical login manager (e.g. GDM)
* vtconsoles (i.e. the text mode console displayed shortly after the bootloader)
* EFI framebuffer driver (i.e. the framebuffer used by the BIOS and bootloader)
* `nvidia` drivers

There is a small race condition between unbinding the existing drivers and being able to unload the kernel module; sleeping for 12 seconds seems to be the sweet spot, but (at least according to other guides) this can vary depending on hardware.

At this point the GPU can be rebound to the `vfio-pci` driver which handles the passthrough to the VM.  After the VM stops, we reverse this process.  This is where the hook scripts come into play; they automate the handling of this process when the VM starts and stops.

Note that much of this is cargo-culted from other guides and resembles the configs found there.  You may need to change the PCI IDs below or things such as the name of the graphical login manager service.

Create the following dirs/files and `chmod +x` all of them:

`/etc/libvirt/hooks/kvm.conf`:

```
/etc/libvirt/hooks/kvm.conf 
#!/bin/bash

# TODO: Replace these with your real GPU PCI IDs, replacing punctuation with underscores (use lspci to determine these)
VIRSH_GPU_VIDEO=pci_0000_0b_00_0
VIRSH_GPU_AUDIO=pci_0000_0b_00_1
```

`/etc/libvirt/hooks/qemu.d/win10/prepare/begin/start.sh`:

```
#!/bin/bash
set -x

# PCI IDs and other constants
source "/etc/libvirt/hooks/kvm.conf"

# Stop GDM and kill all sessions
systemctl stop gdm.service
killall gdm-x-session

# Unbind vtconsoles
echo 0 >/sys/class/vtconsole/vtcon0/bind
echo 0 >/sys/class/vtconsole/vtcon1/bind

# Unbind EFI framebuffer
echo efi-framebuffer.0 >/sys/bus/platform/drivers/efi-framebuffer/unbind

# Sleep to avoid a race condition
sleep 12

# Unload nvidia drivers
modprobe -r nvidia_drm
modprobe -r nvidia_modeset
modprobe -r drm_kms_helper
modprobe -r nvidia
modprobe -r i2c_nvidia_gpu
modprobe -r drm

# Unbind the GPU
virsh nodedev-detach $VIRSH_GPU_VIDEO
virsh nodedev-detach $VIRSH_GPU_AUDIO

# Load vfio driver
modprobe vfio
modprobe vfio_pci
modprobe vfio_iommu_type1
```

`/etc/libvirt/hooks/qemu.d/win10/release/end/revert.sh`:    

```
#!/bin/bash
set -x

# PCI IDs and other constants
source "/etc/libvirt/hooks/kvm.conf"

# Unload vfio driver
modprobe -r vfio_pci
modprobe -r vfio_iommu_type1
modprobe -r vfio

# Bind the GPU
virsh nodedev-reattach $VIRSH_GPU_VIDEO
virsh nodedev-reattach $VIRSH_GPU_AUDIO

# Bind vtconsoles
echo 1 >/sys/class/vtconsole/vtcon0/bind
echo 1 >/sys/class/vtconsole/vtcon1/bind

# Let nvidia re-detect GPU
nvidia-xconfig --query-gpu-info &>/dev/null

# Bind EFI framebuffer
echo efi-framebuffer.0 >/sys/bus/platform/drivers/efi-framebuffer/bind

# Load nvidia drivers
modprobe nvidia_drm
modprobe nvidia_modeset
modprobe drm_kms_helper
modprobe nvidia
modprobe i2c_nvidia_gpu
modprobe drm

# Start GDM
systemctl start gdm.service
```

Skip down to *Finishing the setup* below since the next section isn't applicable if you are only using one GPU.
