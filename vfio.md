# "Seamless" Windows 10 on Linux with KVM and VFIO

This serves to document my experience setting up "seamless" Windows 10 for near-native performance using KVM and VFIO (i.e. dedicated graphics card for each OS, with both OSes being usable at once on the same desktop, and reasonable integration such as straightforward file sharing as well as input/clipboard sync).

I assume that you know your way around both Linux and Windows and have a reasonable understanding of the various Linux features and config files being used, as well as how they affect the underlying hardware.

## Getting started

### Preface

It's worth noting that I did not document this as much as I should've early on, much of this was figured out as I went and is being recalled from memory (or at least system logs from running `pacman`, `sudo`, etc).  I also tried a few different configurations (initially, one of the single GPU passthrough guides, though I decided I didn't want to deal with the overhead of quitting all Linux applications to switch to Windows and vice versa and had another card to use).  I try to cut some of these "false starts" out of this guide, and just focus on the dual GPU case, but there are probably parts that are plain wrong; do your own research instead of trying to blindly follow what is written here.

### Hardware (at least what's relevant to VFIO)

* AMD Ryzen 9 5950X
* 64GB RAM
* Mobo: ASUS ROG Crosshair VIII Dark Hero (AMD X570 chipset)
* Storage: 2TB NVMe (Gen4), 1TB NVMe (Gen3), 1TB SSD (SATA)
* GPUs: Nvidia GTX 1070, Nvidia RTX 3090
* Displays: 2x 4K@60

Note that I already had Win10 running successfully from the SATA SSD, with only the 3090 installed.  All Windows and BIOS updates had been applied to the system before starting.

### Goals

I found it helpful to lay out what my goals/desired use-cases were ahead of time:

* Being able to game on a Windows VM at performance approaching bare metal.  (I got a 3090 so that I could game at 4K@60 comfortably on max/near-max settings; this was achievable in most games on Win10 running on bare metal).
* Fast software dev/build cycles for both Linux and Windows.  For example I need to be able to build complex packages such as LLVM on Windows, a build of Clang and LLVM took ~12 min on baremetal Win10 with this hardware).  As part of this, I'd prefer to do the actual "writing code" part on Linux and leave Windows for running/testing/debugging software as much as possible.
* Reasonably "seamless" integration between the two OSes.  e.g. being able to move my cursor between the Linux and Windows desktop, straightforward clipboard/file sharing, etc.  [This Reddit post](https://www.reddit.com/r/VFIO/comments/q00vw7/complete_unification/) in particular served as inspiration.
* Reasonable performance in both the host and the VM while the VM is running (i.e. doing CPU/IO intensive stuff in the VM shouldn't cause the rest of the system to bog down and vice versa).  That said, I'm not terribly concerned with super high performance 3D graphics on Linux.

After some research along with trial-and-error, I made the following decisions as I went:

* Linux distro: Arch (in particular, I like the rolling binary release model as it reminds me of Homebrew on macOS, and the fact that the distro doesn't really try to shoehorn you too much into a particular set of base software, window manager/desktop environment, etc).  That said, it assumes a lot of Linux background and patience for manual setup; I would *not* recommend Arch to someone who is new to Linux or just wants everything to "just work" out of the box without tinkering.
* More or less 50/50 split between Linux and Windows hardware-wise (with the exception of storage; I'd prefer that storage be seen as one large abstract pool of resources for the most part to minimize duplication of data between OSes and wasted space).  Relatedly I decided I'd be fine with giving a single monitor to Windows for now.
    * The 1070 is my boot GPU to use with Linux, 3090 is passed through to the Windows VM
    * Windows gets roughly half the CPU and RAM.  This is straightforward with Zen 3 CPUs like the 5950x, which has two 8x/16t CCXs each with its own L3 cache, so the Windows guest will get one of the CCXs.
    * No host isolation as seems to be commonly configured on VFIO setups; I want the host to be able to use all the cores/RAM when the VM isn't running.

## Linux installation

From within Windows, I copied Arch to a USB drive and followed the general Arch [installation guide](https://wiki.archlinux.org/title/Installation_guide) to get at least a basic system setup.  Particular things I remember about this, in rough order as you follow the guide:

### Filesystem and bootloader setup

* I used my (larger) 2TB NVMe drive for Linux since I figure this is where software is going to be getting compiled, etc.
* I decided to make the EFI system partition 550MiB just in case as suggested by some guides (or if I ever decide to play with different EFI binaries!) and have it mounted in `/efi`; everything else in `/`, including `/boot`, resides on the rest of the NVMe drive (i.e. second partition).
* I haven't configured swap seeing as how I have 64GiB of RAM and don't really care about suspend to disk (assuming that would even work properly in a VM with passthrough hardware).
* I used GRUB as the boot loader (mainly since that's what I'm familiar with); enabled early microcode loading via `amd-ucode`; installed a few other random tools such as `efibootmgr`, `vim` (my preferred console editor), etc before I rebooted to be sure I'd have them if network didn't work.

### Networking 

The 1Gbps network interface was autodetected as `enp6s0`.  DHCP worked in the installer, but `dhcpcd` had to be manually installed and turned on after rebooting (first temporarily setting a static IP to get pacman working):

```
# ip link set enp6s0 up
# ip addr add 10.0.0.41/24 broadcast + dev enp6s0
# vim /etc/resolv.conf  # added "nameserver 8.8.8.8"
# pacman -S dhcpcd
# systemctl enable --now dhcpcd@enp6s0.service
```

### Sudo and user setup

Now that we can install packages, configure sudo so we don't have to do everything in a root shell.

```
# pacman -S sudo
# EDITOR=vim visudo
```

In the sudoers file:

```
# in the "user privilege specification section at bottom:

# so we don't have to keep specifying EDITOR=vim
Defaults env_keep += "EDITOR"

# this line is already there, but commented out
# allow anyone in group wheel to run any command as root
%wheel ALL=(ALL) ALL
```

Create an user account for yourself and make it a member of the `wheel` group, so it can use sudo to run commands as root, then log in so we can continue as that user:

```
useradd -m blackbox
passwd blackbox  # add a password so you can actually login
gpasswd -a blackbox wheel
exit
```

### Graphics server and login manager setup

This continues to roughly follow the Arch [general recommendations for a GUI](https://wiki.archlinux.org/title/General_recommendations#Graphical_user_interface).

I decided to stick with X since that's what I'm familiar with, it's been around a long time, and there are a few glaring issues in wayland that don't seem fixed yet.

### Installing X and graphics drivers

We'll be using the proprietary NVIDIA drivers since the opensource ones don't play well with unbinding and rebinding to the VM.  Install X and the NVIDIA drivers (nvidia-dkms probably isn't necessary, since we're not using a custom kernel):

```
$ sudo pacman -S xorg xorg-apps nvidia nvidia-utils nvidia-dkms nvidia-settings
```

### Text mode graphics fixes

We also set up early modesetting and tweak some GRUB settings to make the early boot-up display slightly nicer, instead of some ugly legacy graphics mode EFI puts us into:

`sudo vim /etc/default/grub` and change the following settings:
* `GRUB_CMDLINE_LINUX_DEFAULT`: add `nvidia-drm.modeset=1` to the end of the list of kernel parameters to enable KMS (i.e. use native resolution and better fonts in the text console before the display server starts)
* `GRUB_GFXMODE=3840x2160x32` (or whatever the native res of the boot monitor is)
* `GRUB_GFXPAYLOAD_LINUX=keep` to avoid unnecessary video mode switching which can cause flickering/glitching/black screen issues.

`sudo grub-mkconfig -o /boot/grub/grub.cfg` to regenerate the config file.  Rebooting should show the bootloader switching into the native resolution right away.

### Desktop environment

After a whole series of false starts, I ultimately ended up going with [Xfce](https://wiki.archlinux.org/title/Xfce) both because I've used it in the past and it seems to be less buggy/feature-lacking compared to a lot of other DEs.

I ended up installing GNOME and MATE and using some components from those WMs as well.  [My full desktop setup process is covered here](desktop.md) since this isn't really VFIO specific.

### Misc stuff

I installed `yay` from AUR to make it easier to install AUR packages without manual build steps.

I installed some other misc tools like cpu speed, sensors tools: `cpupower`, `nvme-cli`, `hdparm`, `util-linux`, `lm_sensors`.

I had problems getting sensors/fan control to work well for this mobo (apparently there is a patch in the works that may make it upstream, for `asus_wmi`) with the intention of monitoring hardware temps, fan speeds etc, but gave up for now and just configured reasonable fan settings in the BIOS.

I also installed `ntfs-3g` for read/write support to NTFS partitions in Linux (since Windows is still installed on the SSD!) and a handful of proprietary apps using their AUR packages (e.g. Chrome, Spotify, Discord, VS Code, etc).

## Setting up the virtual machine

* Before you start, you should also make note of your IOMMU groups as all devices in a group have to be passed through together.  Luckily, at least on the X570 chipset the IOMMU groups are pretty good (for example both PCIe slots connected to the CPU's lanes are in their own IOMMU groups).  You can see which devices are in which IOMMU groups using the following script:
    ```
    shopt -s nullglob
    for g in `find /sys/kernel/iommu_groups/* -maxdepth 0 -type d | sort -V`; do
        echo "IOMMU Group ${g##*/}:"
        for d in $g/devices/*; do
            echo -e "\t$(lspci -nns ${d##*/})"
        done
    done
    ```

    For example, on my machine, this prints the following:
    ```
    ...snip for brevity...
    IOMMU Group 26:
        0a:00.0 VGA compatible controller [0300]: NVIDIA Corporation GP104 [GeForce GTX 1070] [10de:1b81] (rev a1)
        0a:00.1 Audio device [0403]: NVIDIA Corporation GP104 High Definition Audio Controller [10de:10f0] (rev a1)
    IOMMU Group 27:
        0b:00.0 VGA compatible controller [0300]: NVIDIA Corporation GA102 [GeForce RTX 3090] [10de:2204] (rev a1)
        0b:00.1 Audio device [0403]: NVIDIA Corporation GA102 High Definition Audio Controller [10de:1aef] (rev a1)
    ...
    ```

    If the GPU devices (i.e. the video controller and associated HDMI audio controller) aren't in the same IOMMU group, you can try setting up ACS override (out of scope here, since this is not necessary on the X570 chipset).

* I used Win10 Enterprise; if you don't have Windows, getting a copy is out of scope of this guide but you might be able to download an ISO from Microsoft.

* To save some time, download some drivers and tools we will need in the guest OS, as some of these are relatively big downloads and are hosted on slow servers:
    * [Latest virtio-win ISO](https://github.com/virtio-win/virtio-win-pkg-scripts/blob/master/README.md) - the other items can be ignored, these are all present in the ISO.
    * [spice-guest-tools](https://www.spice-space.org/download.html) - under Guest > Windows binaries.

### Basic VM configuration

The primary guide I followed was the Arch wiki's [PCI passthrough](https://wiki.archlinux.org/title/PCI_passthrough_via_OVMF) guide which goes through the steps of setting up a Windows VM.  I'll paraphrase and make some adaptations here as we go.

Install libvirt and dependencies, and start up its default network (i.e. guests connected via NAT through the host):
```
sudo pacman -S qemu edk2-ovmf virt-manager ebtables dnsmasq
sudo systemctl enable --now libvirtd.service virtlogd.socket
sudo virsh net-autostart default
sudo virsh net-start default
```

Start `virt-manager` (either from the command line or your window manager's menu).  Create a new VM with the following settings:

* Choose Manual install (as opposed to the options to install from CDROM)
* Type `Microsoft Windows 10` and select from the OS list
* Set Memory and CPUs to around half of your physical hardware (e.g. I used `32768 MiB` (32GiB) and 16 CPUs)
* Create a disk image for this virtual machine.   Make it reasonably large; I went with `250 GiB` to start.
* Set the name to something suitable, I went with `win10` (you will be typing this repeatedly on the command line so using long names or spaces is discouraged).
* Check `Customize configuration before install` and click Finish.

The new VM's configuration screen will pop up.  Configure the hardware further here to default to the `virtio` drivers and set a few other important settings through the UI:
* On the `Overview` tab, set Chipset to `Q35` and Firmware to `UEFI x86_64: ...some path here...`
    * If there is more than one UEFI firmware to choose from, I just went with the non-secure boot image (the one without `secboot` in the name, which is fine for Win10.
    * Click apply before leaving each screen.
* Change the disk bus for `SATA Disk 1` (the volume you just created) to `VirtIO`.
* Change the NIC model to `virtio`
* Remove the `Tablet` device
* Change `Video` model to `VGA` (some of the others might work but this seems to be the most compatible, especially if you need to debug further in the VM).
* Add a storage device of type `CDROM device` and point it to the Windows 10 ISO using the `Manage` button.
* Add another CDROM device and point it to the `virtio-win.iso` you just downloaded. Don't configure anything in `Boot Options` yet, we don't want the VM to start installing Windows yet!

At this point, you can click the green checkmark Complete Installation button in the upper left.  The VM will be (unhelpfully) started and probably arrive at an EFI shell command prompt.  Press `Left Ctrl` and `Left Alt` (to ungrab console input, if the VM console captured input) and shut the VM down via Virtual Machine > Shut Down > Force Off.

Switch back to the settings screen (lightbulb icon) and configure a few more settings:
* Under `CPUs` uncheck Copy Host CPU configuration and change model to `host-passthrough` (you may have to type this in).
* Under `Boot Options` enable `SATA CDROM 1` so that we can boot the Windows installer.

There are a few more settings needed that can only be edited through the VM's XML file, so do that now via `sudo EDITOR=vim virsh edit win10` and change the following:
* `<os>` section: change `<loader>` `type='rom'` (otherwise snapshots won't work!)
* `<cpu>` section: set `check='none'`, additionally, for Ryzen CPUs, add a new child element `<feature policy='require' name='topoext'/>` (this is required for SMT to work at all).
* Change both `<input>` sections for types `keyboard` and `mouse` to use `bus='virtio'`.

Once the file is saved and the editor closed, the `virt-manager` settings UI should automatically update based on the changes.  (I noticed that an extra keyboard/mouse appeared after this; this appears to be harmless and represents the fact that PS/2 emulation is used until the virtual input drivers are started).

### Installing Windows in the VM

At this point the system is configured with a few basic devices and a few paravirtualized ones using `virtio`; we should be ready to install Windows now.

Click the screen toolbar icon to switch back to the virtual console, then start the VM with the play icon.  As the VM is starting, click in the window (to capture keyboard input) and press a key once the `Press any key to boot from CD or DVD...` prompt appears (if you're too slow, force reset the VM and try again).  Forcing scaling (under View > Scale Display > Always) can help with readability of the text on screen once the installer boots (since we are stuck in VGA mode for now).

In my experience loading the installer sometimes hung for ~30 seconds at the TianoCore screen, but eventually started.

Click through the first few pages of the installer, settings I used were:
* Language: English (US)
* Edition: Win10 Enterprise
    * If asked for a product key, choose to enter it later.  We'll worry about activation once all the hardware setup is complete, as the changes we'll be making for VFIO might affect activation later.
* Click `Custom: Install Windows only`

The next screen won't show any drives to install to (yet) since the `viostor` driver isn't yet loaded. Click `Load driver`, press `Browse`, and browse to the `amd64\w10` dir on the virtio drivers CD drive (`E:` if you set it up as above).  You should see an entry for `Red Hat VirtIO SCSI controller` (viostor.inf) appear; press Next to load the driver and the virtual HDD should now show up.

The rest of the installation should be fairly uneventful and proceed like any other Windows 10 install.  Note that there won't be any networking due to the `NetKVM` driver not yet being installed, so you will have to skip through most of the "first boot" options and won't be able to link the first user account to a Microsoft account, not that I really care to do so anyway.  Eventually you should be presented with the Windows desktop, albeit at a horrible resolution (as a reminder, `LCtrl + LAlt` is the default keybind to un-capture the keyboard and mouse).

### Basic Windows configuration

Eventually we'll end up at the desktop, albeit with horrible video resolution, but we can finish installing the rest of the basics.

Open Device Manager (right click Start icon > Device Manager).  Much like in the previous section, we will be installing drivers from the VirtIO drivers CD.  At least in my setup, I see 5 or so devices missing drivers, follow this procedure to install each:

* Right click and select `Update driver`
* `Browse my computer for drivers`
* Browse to the appropriate place on the virtio drivers CD for each one (again, assuming the previous configuration, the drivers should be in drive `E:`):
    * Ethernet Controller: `E:\NetKVM\w10\amd64`
    * PCI Device: `E:\Balloon\w10\amd64` (this is the "memory balloon" which allows the host to reclaim memory from VMs)
    * PCI Keyboard Controller / PCI Mouse Controller: `E:\vioinput\w10\amd64`
    * PCI Simple Communications Controller: `E:\vioserial\w10\amd64`

Reboot the VM.  At this point there should be functioning networking and ever so slightly more responsive input devices.  Now is probably a good time to configure other Windows settings, install any other system tools you want in the VM, etc.  (don't bother installing anything graphically intensive yet, even modern webbrowsers make solid use of GPU acceleration and will perform like garbage for now).

At this point, it should be safe to edit the VM configuration and remove the CDROM devices, since we're done with the driver installation for now.

### Sharing files with the guest

Samba is probably the simplest (and in my experience, fastest) way to share files from the host to the guest.  To set this up, I followed the directions on the Arch wiki: [Samba](https://wiki.archlinux.org/title/Samba).  Or in short:

* Install: `sudo pacman -S samba`
* Pasted the [sample config](https://git.samba.org/samba.git/?p=samba.git;a=blob_plain;f=examples/smb.conf.default;hb=HEAD) into `/etc/samba/smb.conf` (the predefined `[homes]` share was good enough for me, which allows me to access my Linux homedir from Windows)
* Enabled the services: `sudo systemctl --now smb.service nmb.service`
* Created an SMB password for my Linux account: `sudo smbpasswd -a blackbox`
    * Ideally this should be different from your Linux account password; apparently Windows/Samba/whatever uses some really archaic hash algorithm for passwords

At this point I could access my home directory within the Windows VM by opening an explorer dialog and browsing to the SMB share using the host name/IP directly (i.e. `\\10.0.0.39` in my case), inputting the credentials I set up with `smbpasswd` and then clicking on my username (the homedir share).

### Setting up spice-guest-tools (i.e. clipboard sync)

Now that it's possible to grab arbitrary files from the host via the Samba share, I installed `spice-guest-tools` in Windows with the `spice-guest-tools-latest.exe` that was downloaded previously to get clipboard synchronization working (and also seemed to result in smoother mouse movement).

At this point you should have a basic (albeit poorly-graphically-performing) Windows install ready to use for VFIO passthrough.

### Other (less-obvious) Windows tweaks/fixes

* I disabled Windows Defender (not only is it not really a good AV, due to a huge number of false-positives, including with software such as Looking Glass that is used below in the dedicated-GPU passthrough below; use NOD32 instead if you want to run an AV software), it's probably dead weight in a VM that (presumably) you are only going to use for gaming and not as a daily driver.  Steps I took to disable it (at least on Enterprise; in consumer versions of Windows, the group policy editor is not available IIRC):
    * Set the following GPOs via `gpedit.msc`:
        * Computer Configuration\Administrative Templates\Windows Components:
            * File Explorer
                * Configure Windows Defender SmartScreen = Disabled
            * Microsoft Defender Antivirus
                * Turn off Microsoft Defender Antivirus = Enabled
            * Microsoft Edge
                * Configure Windows Defender SmartScreen = Disabled
            * Windows Defender SmartScreen
                * Explorer
                    * Configure App Install Control = Disabled
                    * Configure Windows Defender SmartScreen = Disabled
                * Microsoft Edge
                    * Configure Windows Defender SmartScreen = Disabled
    * Force a GPO update to apply these settings by running `cmd.exe` as administrator, `gpupdate /force`
    * In addition, I disabled the `WinDefend` service entirely (may be your only option on Win10 Pro), which is painful to do because normally it's protected from even admin command prompts:
        * Download [AdvancedRun](https://www.nirsoft.net/utils/advanced_run.html) to allow running Windows programs with "unusual" settings/credentials
        * Right click `AdvancedRun.exe`, run as admin (if you don't, nothing will happen), then set the following settings:
            * Program to Run: `cmd.exe`
            * Run As...: select `TrustedInstaller`
            * Click Run and a command prompt should open
            * Run the following in the TrustedInstaller command prompt:
                * `sc config WinDefend start= disabled` (the space after `start=` matters)
                * `sc stop WinDefend`

* Install Windows Terminal from the Windows Store, it includes tools such as `ssh` and in general looks much nicer than the built in command prompt.

At this point, I think we're ready to get graphics and sound working "properly" rather than using the emulated VGA graphics.  Shut down the VM since we need to make more (drastic) changes to the configuration.

## VFIO "pre-setup"

As discussed above, this assumes that you are on an X570 motherboard (i.e. your GPU and associated HDMI sound) is in its own IOMMU group.

### Enable IOMMU (but only for passthrough devices)

Add `iommu=pt` to your kernel parameters.  In my case, since I'm using GRUB on Arch, I edited `/etc/default/grub` and added `iommu=pt` to `GRUB_CMDLINE_LINUX_DEFAULT` (prior to other additional kernel params such as `nvidia-drm.modeset`).
* After doing this, regenerate the GRUB config file with `sudo grub-mkconfig -o /boot/grub/grub.cfg` then reboot with the new settings.
* It's worth noting this is necessary to avoid hurting host OS performance for other devices in the system (that aren't being passed through to the VM), it causes the kernel to enable IOMMU only for passthrough devices. Some other guides seem to recommend `amd_iommu=on` (or `intel_iommu=on` for Intel CPUs) but this doesn't seem necessary (on Arch at least) and is probably slower than this option. ([reddit thread](https://www.reddit.com/r/Proxmox/comments/hhx77k/the_importance_of_iommupt_with_gpu_pass_through_i/))

### Hook scripts pre-setup

`libvirt` allows for executing a [hook script](https://libvirt.org/hooks.html) at various points in the VM lifecycle.

This is somewhat unwieldy however since everything gets directed to `/etc/libvirt/hooks/qemu`; there is a [helper script](https://passthroughpo.st/simple-per-vm-libvirt-hooks-with-the-vfio-tools-hook-helper/) to make it easier to define arbitrary hooks, TL;DR (if you trust the contents of the script):

```
sudo mkdir -p /etc/libvirt/hooks
sudo wget 'https://raw.githubusercontent.com/PassthroughPOST/VFIO-Tools/master/libvirt_hooks/qemu' \
     -O /etc/libvirt/hooks/qemu
sudo chmod +x /etc/libvirt/hooks/qemu
sudo systemctl restart libvirtd.service
```

Now we can define individual hook scripts in the form of shell scripts in various directories, e.g.:
* `/etc/libvirt/hooks/qemu.d/$VM_NAME/`
    * `prepare/begin/*.sh`
    * `release/end/*.sh`
    * `started/begin/*.sh`
    * `stopped/end/*.sh`

### Have guest send audio to user's PulseAudio daemon

**NOTE:** I do not claim that this is the best way to configure PulseAudio or anything about its security. By all means please challenge me on this. Getting sound working was genuinely a pain.

I took a little detour here because sound didn't seem to be working in the Windows guest.  Generally speaking, the pulseaudio sound server gets started per-user session on most Linux distributions, Arch is no different.  The VM needs to be started as root (to access KVM) and then drops to `nobody` unless configured otherwise which complicates sending sound to the active user's pulseaudio daemon.

To work around this, I configured libvirt / qemu to send sound directly to my pulseaudio daemon, along with a few other tweaks:

Change the audio configuration in the `<devices>` section of the VM XML file (assuming I, `blackbox` is uid `1000`, check with the `id` command and change these accordingly):
```
$ sudo virsh edit win10

<sound model='ich9'>
    <codec type='micro'/>
    <audio id='1'/>
</sound>
<audio id='1' type='pulseaudio' serverName='/run/user/1000/pulse/native'/>
```

I enabled anonymous auth for pulseaudio in `/etc/pulse/default.pa` (TODO: not sure if necessary?):
```
load-module module-native-protocol-unix auth-anonymous=1
```

However I bashed my head against the wall for quite a while not understanding why it still wasn't working.  The file permissions on my `/run/user/1000` dir weren't allowing the VM to connect.  I added the following hook script in `/etc/libvirt/hooks/qemu.d/win10/prepare/begin/fix_pulseaudio_perms.sh`:

```
#!/bin/bash
# ensure the VM has access to the pulseaudio server file
chmod go+x /run/user/1000 /run/user/1000/pulse
```

As I discovered later on, I also needed to make the following changes to `/etc/libvirt/qemu.conf` in order to still have audio once the virtual GPU is disabled.

**TODO:** I'm unclear if the `user`/ `group` is actually used or needed here.  It didn't seem to make much of a difference (but maybe worked after a reboot?)

```
nographics_allow_host_audio = 1
user = "blackbox"
group = "blackbox"
```

Restart libvirtd with `sudo systemctl restart libvirtd.service`.

### Bypass VM detection for GPU drivers

Note: I'm not sure if this is actually required anymore now that Nvidia now officially supports GPU passthrough for non-workstation cards, but I did it anyway for sake of completeness, as I guess it is still required in older drivers.  I guess it is still required for AMD GPUs.

Apparently some (at least older) versions of Nvidia drivers check whether the system is running in a VM and refuse to run, causing a black screen at boot.

`sudo virsh edit win10` and make the following changes:

* In the `<hyperv>` section under `<features>`, add `<vendor_id state='on' value='whatever'/>` (`whatever` can literally be any random value).
* Add a `<kvm>` section just below the `<hyperv>` section with the following:
    ```
    <kvm>
      <hidden state='on'/>
    </kvm>
    ```

### Set up Looking Glass on the Linux host

As you may have read in the Arch wiki PCI passthrough guide, [Looking Glass](https://looking-glass.io/) is a neat piece of software that mirrors the video output from the Windows VM and the Linux desktop with minimal latency, it does so via a Windows service that captures the GPU framebuffer and passes it to a program on the Linux host through a shared memory device.  It also handles mouse/keyboard input and clipboard sync via Spice (the same method that is used to display the VM in `virt-manager`).  This avoids the need for switching monitor inputs and passing through USB devices in order to interact with the Windows system.

Install the Looking Glass client (confusingly named, the "host" program runs on the Windows VM) on Linux.  On Arch you can just do `yay -S looking-glass` (or you can compile from source if you want).  At time of writing, the stable version of Looking Glass that I used was B4.

(note: the Looking Glass website suggests compiling and using the `kvmfb` driver if your (host) video card supports dmabuf to avoid an extra CPU memory copy, rather than following the steps below to set up `ivshmem`, but nvidia drivers don't seem to support this)

Next, we need to set up the shared memory file that will be used to pass the Windows framebuffer to Linux.  Create `/etc/tmpfiles.d/10-looking-glass.conf` with the following (note the spaces are actually tabs):

```
f   /dev/shm/looking-glass  0660    blackbox    kvm -
```

(of course replacing `blackbox` with your username).  Create the file now without having to reboot with: `sudo systemd-tmpfiles --create /etc/tmpfiles.d/10-looking-glass.conf`

### Set up Looking Glass in the VM

With the VM still shut down, edit the VM XML file again to add the shared memory device at the end of the `<devices>` section:

```
$ sudo virsh edit win10

<shmem name='looking-glass'>
  <model type='ivshmem-plain'/>
  <size unit='M'>256</size> 
</shmem>
```

The size in the `<shmem>` entry needs to be configured based on your target resolution and according to the following formula (taken from the Arch wiki): `size in MiB = ((screen width * screen height * 8) / 1048576) + 10`, rounded up to the nearest power of 2.

I made the naive assumption that my Windows desktop could be 2 4K displays put together, giving a 7680x2160 screen size.  Applying the preceding formula works out to the 256 MiB required.  (In retrospect, I think this could be smaller, since I'm only using one display for now).

Boot the VM back up within `virt-manager`.  Once it's booted, the next step is to install the `ivshmem` driver in Windows to allow Looking Glass to access the shared memory device we just set up.

For some reason, this driver isn't included in the virtio drivers ISO, so you need to download the [signed driver from Red Hat](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/upstream-virtio/) and extract it on the Windows system.  I went with `virtio-win10-prewhql-0.1-161.zip` at time of writing.

Right click the Start icon and open Device Manager. Under the `System devices` section, find the `PCI standard RAM Controller` device (it will not have the yellow warning triangle, it looks like a properly configured device since Windows silently installs a dummy driver for it), right click on it, select update driver, and browse for the extracted `ivshmem` driver (it should be found under the `Win10\amd64` folder in the zip file).

Now we can install the [Looking Glass host](https://looking-glass.io/downloads) from the website (again, I used stable version B4).  Note that the program is falsely flagged by virus scanners, and the zip file is passworded, using the commit hash listed above the download links as the password.  There is little configuration to do here; the option to create a desktop shortcut can be skipped because it will be installed as a service.  Note that this program will do absolutely nothing until we finish setting up the graphics card.

After this is installed, shut the VM down again.  We are now pretty much ready to setup VFIO and passthrough the GPU to the VM.

### Monitor setup/cabling

At least in the case of the 3090 (but I expect this is probably true for a lot of cards) the card needs to be able to detect at least one EDID (i.e. active monitor connection), or the GPU won't initialize when the guest boots up, which will prevent Looking Glass from starting and Windows will be pretty much inaccessible.

As a workaround for now, I connected an HDMI cable from the 3090 to one of my monitor's unused inputs.  Unfortunately, this also meant I needed to switch to that input when (re)starting the VM for the GPU to detect a display.  I got tired of switching inputs by hand very quickly and ended up [automating input switching using ddcutil](https://passthroughpo.st/switching-monitor-inputs-software-ddc/).

That said, this was still clunky since it required switching away from the Linux desktop for a few seconds during each VM reboot.  I ended up buying [EDID dummy plugs on Amazon](https://www.amazon.com/dp/B07FB4VJL9) ($10 USD for two) and plugged one into the 3090 so it will always think a monitor is connected.

### Early load the vfio-pci driver and bind the guest GPU to it

The Linux `vfio-pci` driver is effectively a "stub" driver that serves only to prevent other drivers from binding to a device (and manages a few other minor things like PCIe power modes).  As such, we need this driver to load as early in the boot process as possible and bind to the GPU, before the `nvidia` driver can bind it.

Edit `/etc/mkinitcpio.conf` and add the VFIO drivers to the `MODULES=` line:

```
MODULES=(... vfio_pci vfio vfio_iommu_type1 vfio_virqfd ...)
```

These should be placed before any other GPU drivers that may have been early-loaded here, such as `nvidia`, etc).

The Arch wiki also mentions ensuring `modconf` is in the `HOOKS=` entry (it already was for me after `autodetect` and before `block`).

Regenerate the initial ramdisk with: `sudo mkinitcpio -p linux`

Now that the VFIO drivers will load first, we now need to add a kernel parameter to tell the driver which devices to bind.  Find the PCI IDs of your guest video card with `lspci -nn`, for me this resulted in the following output (these are the hex numbers separated with a colon right after the device name in brackets):

```
..snip..
0b:00.0 VGA compatible controller [0300]: NVIDIA Corporation GA102 [GeForce RTX 3090] [10de:2204] (rev a1)
0b:00.1 Audio device [0403]: NVIDIA Corporation GA102 High Definition Audio Controller [10de:1aef] (rev a1)
..snip..
```

Add this to the kernel params in the bootloader config in `/etc/default/grub`, taking care to put it *before* params that affect GPU state such as `nvidia-drm.modeset=1` to avoid those drivers trying to bind to the GPU:

```
GRUB_CMDLINE_LINUX_DEFAULT="... iommu=pt vfio-pci.ids=10de:2204,10de:1aef ..."
```

Remember that all devices in the IOMMU group need to be passed through (i.e. added to the `vfio-pci.ids` kernel param), which include the audio device even if we never use it in the VM.

As before, regenerate the GRUB config with `sudo grub-mkconfig -o /boot/grub/grub.cfg`.  Reboot the machine to use the new initial ramdisk and kernel params.

## Finishing the setup

### Configure passthrough for the GPU in the VM

Go back to `virt-manager` and open the VM configuration screen again.  Click `Add Hardware`, add a PCI host device and select the GPU being passed through (**NOT** the GPU you are using for Linux).

Repeat for anything else in the same IOMMU group, since the entire group has to be passed through together, even if the devices are never used (in my case, this is the GPU itself and the associated HDMI audio device).

Don't remove the virtual VGA video device yet!  (if you do this now, Windows might fail to boot and attempt to enter Startup Repair mode.  Also, if anything goes wrong, we have no way of seeing any video output).

### Install Windows Nvidia drivers and finish Windows configuration

If you are using HDMI dummy EDID plugs like I am, the next steps might be easier to debug with the guest GPU connected to an actual monitor, at least for initial configuration.

Boot the VM back up, log in and wait for a couple of minutes.  Windows should detect the GPU and automatically install drivers (though in some cases I tested, it seems to install an older driver, so you may want to manually download and install the driver from the Nvidia website).  You can open Device Manager to verify that the "real" GPU drivers have been installed (in my case, it shows up as `NVIDIA GeForce RTX 3090` rather than `Microsoft Basic Display Adapter` or something similar).

Right click the desktop, click Display settings, Advanced display settings, and configure the monitor resolution here (at least in my case, Windows defaulted to a resolution that was not the native 4K resolution of the monitor).

Right-click the speaker icon in the system tray, Open Sound settings, and then change the default device to `Speakers (High Definition Audio Device)` (which corresponds to the VM's virtual sound card) as it may have switched to the NVIDIA HDMI sound when the drivers were installed.  In addition, click the Sound Control Panel link on the right under Related Settings and right click > Disable all the NVIDIA sound devices to stop them being used in the future.  Verify the sound works by clicking the speaker icon in the tray and changing the volume (you should be able to hear the "ding" sound from Windows via the Linux host's audio).

(note: I ended up having to do this resolution and sound setup again when I switched from a real monitor to the HDMI dummy plugs, for some reason it seems to create a new sound device for each type of "monitor" detected).

Mouse acceleration should also be disabled (since mouse acceleration is presumably already enabled in Linux, the mouse pointer will feel hard to control and tend to "zoom" too quickly across the screen with this on); right click Start > Settings > Devices > Mouse and then click "Additional mouse options" on the right to open the old mouse control panel. Go to the `Pointer Options` tab and deselect `Enhance pointer precision`.

While I was in this control panel, I went to the Pointers tab and chose the `Windows Black (large)` scheme to more closely resemble the mouse pointer I was using in Linux, and enabled pointer shadows.

After all this is done, shut down the VM again.

### Disable the virtual video device and test Looking Glass

At this point, the "virtual" VGA video device is useless and needs to be configured as a "null" video device since we are using the GPU and Looking Glass to access the VM display (i.e. we want the GPU to be the only active video device in the VM).  With the VM shut down, go to the VM settings screen and click on the `Video VGA` device.  Change the model to `none` (you may need to type this in rather than selecting from the dropdown) and click Apply.  **Do not delete the video device, `Display Spice` or any of the other virtual devices, they are still needed for Spice (i.e. input and clipboard sync) to work!**

Start the VM back up again and verify everything is still working.  In my experience trying this a few different times, it was 50/50 whether things worked immediately or Windows failed to boot.  Rebooting a couple of times (especially if Windows booted to the Startup Repair menu) seemed to get things working again.

Once the VM appears to have booted successfully using only the Nvidia GPU, open a Linux terminal and run `looking-glass-client`.  Assuming everything is working properly, after a couple seconds, you should see an (albeit scaled-down) part of the Windows desktop.

Shut down the VM again (this can be done directly through the Looking Glass window, or the menu in the `virt-manager` window now that the OS is installed.  Note that there won't be any video anymore in `virt-manager` because the VGA device was disabled and Looking Glass takes over the Spice connection anyway).

## "Seamless" integration with Linux

While running `looking-glass-client` directly works, this method is somewhat clunky since we need to start the VM manually first and then run Looking Glass, not to mention the Windows desktop is being scaled down and not filling the screen.

In my case, I wanted Looking Glass to run fullscreen on my second monitor.  We can remedy this with a script and a desktop entry file to make things look a little nicer to use (that is to say, clickable via the desktop's menus/icons rather than having to run commands in the terminal).

### Creating the script

Create `/home/blackbox/bin/start-win10-vfio.sh` and `chmod +x` it (note that I tend to put "user" scripts in `~/bin`, and add this to my `PATH`, so this is only a suggestion, somewhere else such as `/usr/local/bin` would probably work just as well) with the following:

```
#!/bin/bash
if [[ "$(sudo virsh domstate win10)" != "running" ]]; then
  sudo virsh start win10
  sleep 5
fi
looking-glass-client -d -n win:position=3840x0 win:size=3840x2160 win:quickSplash=yes input:rawMouse=yes input:autoCapture=yes &
```

This way, we start the VM first (if it's not running) and launch Looking Glass a few seconds afterwards (the VM needs to transition to "running" state, which takes a few seconds, before Looking Glass will stay alive and wait for a connection to the guest when the OS finishes booting).  This way everything "just works" if the VM is already running but the Looking Glass window was closed for some reason.

Some more explanation of the `looking-glass-client` args:
* `-d` runs the program as a borderless window.  This works well with Xfce's window manager; I found with some other window managers, `-F` seemed to work better.
* `-n` prevents accidental window resizing (e.g. from right click menus or grabbing the edge of the window), since we always want the window to take up an entire monitor.
* `win:position` configures the upper-left corner position of the window (consider that all screens are combined into one big desktop with the upper left corner being `0x0` and the screens are positioned according to X / system settings).  Since I'm using two 4K displays side by side, this corresponds to:
    * `0x0` is the upper left corner of the first monitor.
    * `3840x0` is the upper left corner of the second monitor.  This is the monitor I want it to display on.
* `win:size` configures the width and height of the window, obviously I use `3840x2160` here since I want to match the "native" resolution being used by the GPU.
* `win:quickSplash=yes` stops the splash screen "fade out" effect when the Windows host program connection is started which saves maybe 1-2 seconds.
* `input:rawMouse=yes` sets the mouse to raw input mode when input is captured, which is better for gaming
* `input:autoCapture=yes` is an experiment to see if it works better with capturing mouse input; I'm not sure if this is strictly needed but I include it here for completeness' sake.

We also need to reconfigure `sudo` to allow running `virsh` without a password (since we want a one-click solution that doesn't involve typing passwords); run `sudo visudo -f /etc/sudoers.d/virsh` to create the file, and add the following to let anyone in the `wheel` group (i.e. us) to use `virsh` without a password:

```
%wheel ALL=(ALL) NOPASSWD: /usr/bin/virsh
```

Test the script by running it from the terminal.  After a few seconds the Looking Glass program should launch and the Windows login screen will appear soon after that.

In the case of Xfce (and many other window managers I tested), the WM may override the position and size specified in the args to place the window on the screen the pointer is on.  I ended up using `devilspie2` to override the window position (this is described [here](desktop.md)).  As a somewhat clunkier alternative, you could use `wmctrl` to accomplish the same thing, albeit with a delay for the program to start and display its window:

```
sleep 1
wmctrl -x -r looking-glass-client -e 0,3840,-1,-1,-1
```

### Creating a desktop entry

Linux application menu/launcher shortcuts are stored in `.desktop` files.  To make one for the script we just wrote, create `~/.local/share/applications/StartWin10.desktop` (create the directories if they don't already exist):

```
[Desktop Entry]

Type=Application
Version=1.0
Name=Windows 10
Exec=/home/blackbox/bin/start-win10-vfio.sh
Icon=distributor-logo-windows
Terminal=false
StartupWMClass=looking-glass-client
```

Most of this should be straightforward, but there are a couple of things going on here that are worth mentioning:

* `Exec=` specifies the command to run.  Note that an absolute path is used here since the script is in a non-standard place that probably isn't in the window manager's `PATH`.
* The `Icon=` is a generic Windows icon included with my icon theme (Papirus).  You can also specify an absolute path to a custom icon; relative paths here look up icons from the active theme.
* `StartupWMClass=looking-glass-client` defines the "main" window class, which acts as a hint to the window manager to describe which window is the "main" window (especially for things like pinning on docks/app bars).
    * Running `xprop WM_CLASS` and then clicking a window will tell you the window instance and class name to use here for other programs.  Note that there will be two strings displayed, most WMs (including xfce) use the second one (window class).

Once this file is saved, this should cause your WM to notice it and start showing it in its menus.

### Giving it a try

If you made it this far without running into issues, congratulations, pat yourself on the back, this was a whole lot of trial and error for me.  Now is probably a good time to get some other Windows things out of the way as I did:

* Finish OS activation (now that all the major hardware changes are complete)
* Customize the desktop background/color scheme to match/complement Linux
* Install game clients such as Steam, GOG, whatever.
* Try out some games:
    * With games that use relative mouse movement (which is probably a majority of 3D first/third person based games) you need to press `Scroll Lock` to toggle Looking Glass to (un)capture input, otherwise moving the mouse far enough caused a loss of input (once the cursor was no longer over the Looking Glass window).
    * For some games, performance was not ideal (graphical FPS was fine, but there was a degree of input lag and "micro-stuttering").  This is to be expected since we've not fully set up isolation; the next section deals with how to improve this.
* Holding down `Scroll Lock` will bring up a help menu with other Looking Glass shortcuts.

## Performance improvements

All of these sections assume that you've stopped and restarted the VM after making config XML changes.

### CPU

Each VM thread is represented by a single thread in Linux.  A major source of performance problems is poor cache locality because the host is free to reschedule threads on any available CPU.

On the 5950X there are 2 CCX, each one has 8 cores * 2 threads = 16 CPUs, in addition to an L3 cache.  We'll pin all the cores on the second CCX.  First look up the core/cache topology with `lscpu -e`:

```
CPU NODE SOCKET CORE L1d:L1i:L2:L3 ONLINE    MAXMHZ    MINMHZ      MHZ
  0    0      0    0 0:0:0:0          yes 5083.3979 2200.0000 2919.879
  1    0      0    1 1:1:1:0          yes 5083.3979 2200.0000 2200.000
  2    0      0    2 2:2:2:0          yes 5083.3979 2200.0000 2200.000
  3    0      0    3 3:3:3:0          yes 5083.3979 2200.0000 2200.000
  4    0      0    4 4:4:4:0          yes 5083.3979 2200.0000 2200.000
  5    0      0    5 5:5:5:0          yes 5083.3979 2200.0000 2200.000
  6    0      0    6 6:6:6:0          yes 5083.3979 2200.0000 2200.000
  7    0      0    7 7:7:7:0          yes 5083.3979 2200.0000 2200.000
  8    0      0    8 8:8:8:1          yes 5083.3979 2200.0000 2200.000
  9    0      0    9 9:9:9:1          yes 5083.3979 2200.0000 2200.000
 10    0      0   10 10:10:10:1       yes 5083.3979 2200.0000 2200.000
 11    0      0   11 11:11:11:1       yes 5083.3979 2200.0000 2200.000
 12    0      0   12 12:12:12:1       yes 5083.3979 2200.0000 2200.000
 13    0      0   13 13:13:13:1       yes 5083.3979 2200.0000 3400.000
 14    0      0   14 14:14:14:1       yes 5083.3979 2200.0000 2200.000
 15    0      0   15 15:15:15:1       yes 5083.3979 2200.0000 2200.000
 16    0      0    0 0:0:0:0          yes 5083.3979 2200.0000 3400.000
 17    0      0    1 1:1:1:0          yes 5083.3979 2200.0000 2200.000
 18    0      0    2 2:2:2:0          yes 5083.3979 2200.0000 2200.000
 19    0      0    3 3:3:3:0          yes 5083.3979 2200.0000 2200.000
 20    0      0    4 4:4:4:0          yes 5083.3979 2200.0000 2200.000
 21    0      0    5 5:5:5:0          yes 5083.3979 2200.0000 2200.000
 22    0      0    6 6:6:6:0          yes 5083.3979 2200.0000 2200.000
 23    0      0    7 7:7:7:0          yes 5083.3979 2200.0000 2200.000
 24    0      0    8 8:8:8:1          yes 5083.3979 2200.0000 3400.000
 25    0      0    9 9:9:9:1          yes 5083.3979 2200.0000 2200.000
 26    0      0   10 10:10:10:1       yes 5083.3979 2200.0000 3607.799
 27    0      0   11 11:11:11:1       yes 5083.3979 2200.0000 2200.000
 28    0      0   12 12:12:12:1       yes 5083.3979 2200.0000 2200.000
 29    0      0   13 13:13:13:1       yes 5083.3979 2200.0000 2200.000
 30    0      0   14 14:14:14:1       yes 5083.3979 2200.0000 2200.000
 31    0      0   15 15:15:15:1       yes 5083.3979 2200.0000 2200.000
```

We'll pin the VM's threads to CPUs connected to cache `L3 #1` (i.e. CPUs `8-15` and `24-31` which account for the 8c*2t = 16 threads on the second CCX).

While we're at it, we'll enable IO threading which will be important later for optimizing disk performance.

Both IO threads as well as the qemu emulator (which is used to emulate hardware such as the sound card) correspond to Linux processes that should be isolated from the guest, so we'll pin those to a single core (pair of threads).

`sudo virsh edit win10` and modify the CPU configuration:

```
  <vcpu placement='static'>16</vcpu>
  <iothreads>1</iothreads>
  <cputune>
    <vcpupin vcpu='0' cpuset='8'/>
    <vcpupin vcpu='1' cpuset='9'/>
    <vcpupin vcpu='2' cpuset='10'/>
    <vcpupin vcpu='3' cpuset='11'/>
    <vcpupin vcpu='4' cpuset='12'/>
    <vcpupin vcpu='5' cpuset='13'/>
    <vcpupin vcpu='6' cpuset='14'/>
    <vcpupin vcpu='7' cpuset='15'/>
    <vcpupin vcpu='8' cpuset='24'/>
    <vcpupin vcpu='9' cpuset='25'/>
    <vcpupin vcpu='10' cpuset='26'/>
    <vcpupin vcpu='11' cpuset='27'/>
    <vcpupin vcpu='12' cpuset='28'/>
    <vcpupin vcpu='13' cpuset='29'/>
    <vcpupin vcpu='14' cpuset='30'/>
    <vcpupin vcpu='15' cpuset='31'/>
    <emulatorpin cpuset='0,16'/>
    <iothreadpin iothread='1' cpuset='0,16'/>
  </cputune>
```

While we're at it, we can configure hooks when starting/stopping the VM to use `systemd` to restrict other host processes from using the CPUs that we pinned here for VM use, and undo this at VM shutdown:

`/etc/libvirt/hooks/qemu.d/win10/started/begin/10-restrict-cpus.sh`:

```
#!/bin/bash

host_cpuset="1-7,17-23"

for unit in user.slice init.scope system.slice; do
  systemctl set-property --runtime ${unit} AllowedCPUs=${host_cpuset}
done
```

`/etc/libvirt/hooks/qemu.d/win10/release/end/10-unrestrict-cpus.sh`:

```
#!/bin/bash

host_cpuset="0-31"

for unit in user.slice init.scope system.slice; do
  systemctl set-property --runtime ${unit} AllowedCPUs=${host_cpuset}
done
```

### NIC

Increasing the number of queues (which correspond to the number of `vhost` kernel threads to handle virtualized network IO) seems to drastically improve performance (especially for transfers that use multiple connections).  I went with 8.

`sudo virsh edit win10` and edit the network interface under `<devices>` to set the number of queues:

```
  <devices>
    ...
    <interface type='network'>
      <source network='default'/>
      <model type='virtio'/>
      <driver queues='8'/>
    </interface>
```

I also experimented with `virtio-fs` vs. Samba for sharing files with the host (following the guides [here](https://libvirt.org/kbase/virtiofs.html) and [here](https://virtio-fs.gitlab.io/howto-windows.html) to setup `virtio-fs`).

Not only does Samba seem to have much better performance than virtio-fs, but also more "correct" behavior.  For example, I tried to install Steam games onto the virtio-fs filesystem and ran into issues with files with garbage characters in file names and games not being able to launch from the virtio-fs filesystem.  There doesn't seem to be very much overhead with Samba as network IO is completely virtualized.

### Memory

Enabling hugepages helps with memory latency due to the amount of memory VMs allocate and hold onto for the duration of the VM running (larger pages = fewer page table entries = fewer TLB misses and less time spent walking the page table).

Some other VFIO guides seem to suggest sticking with THP (transparent hugepages) or configuring static hugepages, these have a couple downsides:

* THP has a "warmup" period as it figures out that adjacent physical pages can be coalesced together into hugepages
* Static hugepages prevents other programs that aren't hugepage aware from using them


The huge page size is generally 2 MiB, but let's verify this:

```
$ cat /proc/meminfo | grep Hugepagesize
Hugepagesize:       2048 kB
```

We can compute the number of hugepages needed for the VM based on `VM total memory / huge page size`.  In my case, this would be `32 GiB / 2 MiB = 16384`.

We'll configure dynamic hugepages using the sysctl variable `vm.nr_overcommit_hugepages` (as opposed to `vm.nr_hugepages`, this allows the hugepages to be created only as needed).

`sudo vim /etc/sysctl.d/10-kvm.conf`:

```
vm.nr_hugepages=0
vm.nr_overcommit_hugepages=16384
```

Run `sysctl -p /etc/sysctl.d/10-kvm.conf` to make this setting effective without a reboot.  Now, configure explicit use of hugepages in the VM:

`sudo virsh edit win10`:

```
  <memoryBacking>
    <hugepages>
      <page size='2048' unit='KiB'/>
    </hugepages>
    <access mode='shared'/>
  </memoryBacking>
```

### Storage

The best way to optimize VM storage performance seems to have the least amount of consensus.  I ended up just trying to benchmark this and came to the following conclusions:

* I didn't bother mapping physical block devices or passing through storage devices/controllers using VFIO (i.e. I wanted the flexibility of VM disk images), but I would expect these to be the highest performance storage option.
* `raw` disk images seem to outperform `qcow2` by anywhere from 20-50%, with the downside that online snapshots aren't supported (and must be stored external to the disk image).
* Using `threads` instead of `native` AIO seemed to be slightly faster (apparently the opposite is true for spinning disks, luckily, all my storage devices are NVMe and SSD devices).
* I didn't bother experimenting with different `cache` options, and just stuck with `cache='none'` (i.e. `open()` the image file using `O_DIRECT`, since the guest will be doing its own caching).
* I didn't bother with any other optimizations suggested elsewhere, but will continue experimenting if I run into a bottleneck. 

Convert the qcow2 disk image to raw with: `sudo qemu-img convert -O raw /var/lib/libvirt/images/win10.qcow2 /var/lib/libvirt/win10.img`

Configure the disk devices to use threaded IO, disable host caching, enable passing through TRIM commands, and increase the number of queues (as well as use the raw `.img` created in the last step):

`sudo virsh edit win10`:

```
  <devices>
  ...
    <disk type='file' device='disk'>
      <driver name='qemu' type='raw' cache='none' io='threads' discard='unmap' iothread='1' queues='8'/>
      <source file='/var/lib/libvirt/images/win10.img'/>
      <target dev='vda' bus='virtio'/>
    </disk>
```

I repeated this setup for a second disk that I added later (preallocating disk space ahead of time for the second disk to reduce fragmentation).

## Final tweaks

I removed the virtual CD device (since I don't think I will be needing to use a CD any time soon within Windows).

I did add another 1 TB disk device to the VM, since unfortunately a lot of Windows programs refuse to work, or don't work correctly from a network share.  For everything else (such as old Windows partitions that I still need to copy data from), I created mountpoints for them in `/srv` and mounted them via `ntfs-3g`.